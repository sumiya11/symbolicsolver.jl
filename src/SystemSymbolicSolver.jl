function solve_system(Vars::Vector, Sys::Vector)
    Polyvars = sym_to_polyvars(Vars)[2]
    Polysys = [sympoly_to_poly(Vars,poly) for poly in Sys]

    @info string("Order of solution : ", [string(var) for var in Polyvars[1].parent.S])
    @show groebner(Polysys)
    find_system_roots(Polyvars, groebner(Polysys))
end
