function gcd(a::BigInt, b::BigInt)
    if a == 0
        return b
    else
        return gcd(b%a, a)
    end
end

lcm(a::BigInt,b::BigInt) = (a*b) ÷ gcd(a,b)

function lcm(dens::Vector{BigInt})
    if length(dens) == 1
        return only(dens)
    else
        mid = length(dens) ÷ 2
        lcm(lcm(dens[1:mid]), lcm(dens[mid+1:end]))
    end
end

function normalise_dens!(x::AbstractAlgebra.Generic.Poly{Rational{BigInt}})
    # we are just multiplying the polynomial with
    # smallest factor such that all coeffs. have 1 as denominator
    # We are performing this because poly_factor shows unexpected
    # results for fractional coeffs. Check - x^2 + x + 1//4

    normalisingFactor = lcm(map((q)-> q.den, x.coeffs))
    x.coeffs *= normalisingFactor
    return
end

function m_poly_factor(y)
    normalise_dens!(y)
    return poly_factor(y)
end
