function find_system_roots(vars::Vector, polys::Vector)
    R, x = PolynomialRing(QQ, "x")
    polys_u = to_univariate(R, polys[1])
#     [z^2 - 1]
    # Vector roots
    # [1, -1]
    cur_var_roots = find_roots(polys_u)

#     (x-3)^3 = 0
#     x = 3

    # Vector{Vector}
    polysys_root = Vector{Vector{Rational{BigInt}}}()
    #Base case : only one polynomial
    if length(polys) == 1
        for cur_var_roots_i in cur_var_roots
            push!(polysys_root, [cur_var_roots_i] )
        end
    else
        k = polys[1].parent.num_vars - length(vars)
        #     [
#                z^2 - 1
#                y^2 - z
#                x - 1
#             ]
        # [1, -1]
#         Substitute values of cur_var_roots one by one in each remaining poly
#         Poly[1:n] -> solving Poly[2:n]
#         Merging -> Roots(Poly[1], Poly[2:n])
#
        for cur_var_roots_i in cur_var_roots
            # Hack to substitute the cur_value in equations
            subvars = vcat(vars[1:end-1], cur_var_roots_i,  [0 for x in 1:k])
            # We substitue the value of ith root of current variable in entire system of polynomials
            new_poly = [poli(subvars...) for poli in polys[2:end]]

            # Recursively get roots for next variables return value something like [[1,2,3..],[4,51,2..],...]
            next_polysys_roots = find_system_roots(vars[1:end-1], new_poly)

            # Merge ith root of current variable with all the roots of next variables
            for next_polysys_roots_i in next_polysys_roots
                push!(polysys_root, vcat(next_polysys_roots_i, cur_var_roots_i))
            end
        end
    end
    return polysys_root
end
