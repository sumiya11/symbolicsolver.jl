```@meta
CurrentModule = SymbolicSolver
```

# SymbolicSolver

Documentation for [SymbolicSolver](https://gitlab.com/mananbordia/SymbolicSolver.jl).

SymbolicSolver aims to solve Multivariate Polynomial Systems Symbolically.

Current Approach :
```
1) Get Groebner Basis for the given system of polynomial.
2) The first equation in the Groebner Basis will be univariate. Find the roots of the first equation.
  i) Roots of univariate polynomial equation are found using factorisation.
3) One by one substitute the roots into remaining polynomial equation.
  i) By the nature of Groebner Basis the next (2nd) polynomial will become univariate polynomial.
4) Apply from step 2 and 3 again on the remaining Polynomial System (2nd onwards) recursively.
5) Merge the solution from Univariate Polynomial and Multivariate System of Polynomial to get the solution.    
```
```@index
```

```@autodocs
Modules = [SymbolicSolver]
```
